# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

# response.logo = A(B('FrEECSymp'),XML('&nbsp;'),
#                   _class="navbar-brand",
#                   _id="FrEECSymp")
# response.logo = IMG(_src=URL('static','images/Imperial_Color1.png'), _height='70px')

response.title = 'FrEECS 2019'
response.subtitle = A('The Frontiers in Ecology Evolution & Conservation Symposium, 10-12 September', _style="color: white;")

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'FrEECS Team <s.pawar@imperial.ac.uk>'
response.meta.description = 'FrEECS Symposium website'
response.meta.keywords = 'Student led Ecology, Evolution, Conservation Symposium at Silwood Park, Imperial College London'
# response.meta.generator = 'Web2py Web Framework'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################
# app = request.application

response.menu = [
    (T('HOME'), False, URL('default', 'index'), []),
    (T('REGISTRATION'), False, URL('registration'), []),
    (T('PROGRAM'),False, None, [
            (T('KEYNOTE SPEAKERS'),False,'keynotes'),
#             (T('EVENTS'), False, URL('activities')),
            (T('PROGRAM'), False,'program'),
            (T('SESSIONS'), False,'sessions'),
            (T('PRESENTATIONS'),False,'talks')
        ]),
    (T('FOR PRESENTERS'), False, URL('forpresenters'), []),
    (T('The FrEECS Team'), False, URL('the_team'), []),
    # (T('Database'), False, '#', [
    #    (T('Presenters'), False,URL('all_records'), [])
    #     ])
]
