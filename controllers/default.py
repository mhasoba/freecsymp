# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Welcome to the FrEE Symposyum website")
    return dict(message=T('Welcome to the FrEE Symposyum website'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


def registration():
    update = db.register(request.args(0))
    form = SQLFORM(db.register, update)
    if form.accepts(request,session):
        session.flash = 'Thanks! Your data have been successfully updated' #session object redirects message to program page
        redirect(URL('program'))
    elif form.errors:
        response.flash = 'Please correct error(s) in the form'

    return dict(form=form)

def talks():
    db.talks.id.readable=False
    db.talks.course.readable=False
    grid = SQLFORM.grid(db.talks,
                        csv=False,
                        deletable=False,
                        create=False,
                        details=False,
    #                     maxtextlength=50,
                        maxtextlengths={'talks.speaker':30,'talks.talk_title':120,'talks.FrEECSymp_session':40,'talks.marker_1':25,'talks.marker_2':25},
                        formstyle="divs",
                        paginate=200,
                        editable=False)
    return dict(form=grid)
    # return locals()

def activities():
    return locals()

def program():
    return locals()

def sessions():
    return locals()

def forpresenters():
    return locals()

def keynotes():
    return locals()

def keynotes_prev():
    return locals()

def the_team():#display the FrEECS freaks

    # reorder database rows:
    rows = db(db.FrEEC_team).select(orderby=db.FrEEC_team.last_name)

    return locals()
